package parser;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import structs.Normal3D;
import structs.Point3D;
import structs.TexCoord2D;
import structs.mesh3d.IFSMesh3D;
import structs.mesh3d.TriangularFace;
import structs.mesh3d.Vertex3D;

/**
 * This class models an OBJ-parser. It provides the {@link #parseMesh3D(String)}
 * method to parse a given obj-file to an {@link IFSMesh3D}, as well as the
 * {@link #serializeMesh3D(IFSMesh3D, String)} method to serialize a given
 * {@link IFSMesh3D} back to an obj-file.
 * <p>
 * Supported are any combination of:
 * <li>polygonal faces (polygons will be divided into triangular faces)</li>
 * <li>geometric vertices with (x, y, z) coordinates</li>
 * <li>texture coordinates with(u, v) coordinates</li>
 * <li>vertex normals with (x, y, z) coordinates</li>
 * <li>absence of texture coordinates</li>
 * <li>absence of vertex normals</li>
 * <p>
 * Ignored while parsing are:
 * <li>parameter space vertices</li>
 * <li>any optional w coordinates</li>
 * <li>materials</li>
 * <li>commentary</li>
 * 
 * @author Marwin Rieger 962450
 *
 */
public class OBJParser {
	private static final String EXTENSION = ".obj";
	private static final String DELIMITER = "--------------------------------------------------------------------------------";

	/**
	 * 
	 * Parses the obj-file located at the given path and constructs an
	 * {@link IFSMesh3D}.
	 * <p>
	 * Supported are any combination of:
	 * <li>polygonal faces (polygons will be divided into triangular faces)</li>
	 * <li>geometric vertices with (x, y, z) coordinates</li>
	 * <li>texture coordinates with(u, v) coordinates</li>
	 * <li>vertex normals with (x, y, z) coordinates</li>
	 * <li>absence of texture coordinates</li>
	 * <li>absence of vertex normals</li>
	 * <p>
	 * Ignored while parsing are:
	 * <li>parameter space vertices</li>
	 * <li>any optional w coordinates</li>
	 * <li>materials</li>
	 * <li>commentary</li>
	 * 
	 * 
	 * @param objFile
	 *            path to the file to be parsed.
	 * @return an {@link IFSMesh3D}.
	 * @throws FileNotFoundException
	 *             if the specified file cannot be found.
	 * @throws IOException
	 *             if there is an IO-Error during loading the specified file.
	 */
	public static IFSMesh3D parseMesh3D(String objFile) throws FileNotFoundException, IOException {
		System.out.println("parsing file: " + objFile);
		System.out.println(DELIMITER);

		File file = new File(objFile);
		String path = file.getParentFile() != null ? file.getParentFile().getPath() : ".";
		String name = null;

		try {
			name = file.getName().substring(0, file.getName().lastIndexOf("."));
		} catch (StringIndexOutOfBoundsException e) {
			name = file.getName();
		}

		HashMap<String, Integer> vertexMap = new HashMap<>();

		List<Point3D> positions = new ArrayList<>();
		List<TexCoord2D> texCoords = new ArrayList<>();
		List<Normal3D> normals = new ArrayList<>();

		List<Vertex3D> vertices = new ArrayList<>();
		List<TriangularFace> faces = new ArrayList<>();

		try (BufferedReader in = new BufferedReader(new FileReader(objFile))) {
			String line = in.readLine();
			while (line != null) {
				List<String> splitLine = new ArrayList<String>(Arrays.asList(line.split(" ")));
				splitLine.removeIf(p -> p == null || p.isEmpty() || p.equals(" "));

				if (splitLine.size() != 0) {
					switch (splitLine.get(0)) {
					case "v":
						// found a geometric vertex
						positions.add(parsePosition(splitLine.get(1), splitLine.get(2), splitLine.get(3)));
						break;
					case "vt":
						// found a texture coordinate
						texCoords.add(parseTextureCoordinate(splitLine.get(1), splitLine.get(2)));
						break;
					case "vn":
						// found a vertex normal
						normals.add(parseNormal(splitLine.get(1), splitLine.get(2), splitLine.get(3)));
						break;
					case "f":
						// found a face
						int numVertices = splitLine.size() - 1;
						int[] indices = new int[numVertices];

						for (int i = 0; i < numVertices; i++) {
							if (vertexMap.containsKey(splitLine.get(i + 1))) {
								indices[i] = vertexMap.get(splitLine.get(i + 1));
							} else {
								String[] splitIndices = splitLine.get(i + 1).split("/");

								Point3D p = positions.get(Integer.parseInt(splitIndices[0]) - 1);
								TexCoord2D t;
								Normal3D n;

								if (splitIndices.length < 2 || splitIndices[1].isEmpty()) {
									t = new TexCoord2D(0, 0);
								} else {
									t = texCoords.get(Integer.parseInt(splitIndices[1]) - 1);
								}

								if (splitIndices.length < 3 || splitIndices[2].isEmpty()) {
									n = new Normal3D(0, 0, 0);
								} else {
									n = normals.get(Integer.parseInt(splitIndices[2]) - 1);
								}

								Vertex3D vertex = new Vertex3D(p, t, n);

								int vIndex;
								if (vertices.contains(vertex))
									vIndex = vertices.indexOf(vertex);
								else {
									vIndex = vertices.size();
									vertices.add(vertex);
								}

								vertexMap.put(splitLine.get(i + 1), vIndex);
								indices[i] = vIndex;
							}
						}

						int a = indices[0];
						int b = indices[1];

						// split polygonal faces into triangles
						for (int i = 2; i < numVertices; i++) {
							int c = indices[i];
							faces.add(new TriangularFace(a, b, c));
							b = c;
						}
						break;
					default:
						// found something else
						// for now commentary, materials and parameter space
						// vertices are not being parsed
						break;
					}
				}

				line = in.readLine();
			}
		}

		System.out.println(name);
		System.out.println(vertices.size() + " vertices");
		System.out.println(faces.size() + " faces");
		System.out.println(DELIMITER);

		return new IFSMesh3D(name, path, vertices, faces);
	}

	private static Point3D parsePosition(String sx, String sy, String sz) {
		float x = Float.parseFloat(sx);
		float y = Float.parseFloat(sy);
		float z = Float.parseFloat(sz);
		return new Point3D(x, y, z);
	}

	private static TexCoord2D parseTextureCoordinate(String su, String sv) {
		float u = Float.parseFloat(su);
		float v = Float.parseFloat(sv);
		return new TexCoord2D(u, v);
	}

	private static Normal3D parseNormal(String sx, String sy, String sz) {
		float x = Float.parseFloat(sx);
		float y = Float.parseFloat(sy);
		float z = Float.parseFloat(sz);
		return new Normal3D(x, y, z);
	}

	/**
	 * Serializes the {@link IFSMesh3D} back to an obj-file with the specified
	 * filename.
	 * 
	 * @param mesh
	 *            an {@link IFSMesh3D}.
	 * @param fileName
	 *            the name for the obj-file.
	 * @throws IOException
	 *             if there is an IO-Error during writing the file.
	 */
	public static void serializeMesh3D(IFSMesh3D mesh, String fileName) throws IOException {
		String output;

		if (fileName.endsWith(EXTENSION))
			output = fileName;
		else
			output = fileName + EXTENSION;

		System.out.println("serializing mesh: " + output);

		List<Point3D> positions = new ArrayList<>(mesh.vertices.size());
		List<TexCoord2D> texCoords = new ArrayList<>(mesh.vertices.size());
		List<Normal3D> normals = new ArrayList<>(mesh.vertices.size());

		try (BufferedWriter out = new BufferedWriter(new FileWriter(output))) {
			// fill up the separate lists
			for (Vertex3D v : mesh.vertices) {
				if (!positions.contains(v.position))
					positions.add(v.position);
				if (!texCoords.contains(v.texCoord))
					texCoords.add(v.texCoord);
				if (!normals.contains(v.normal))
					normals.add(v.normal);
			}

			// serialize the geometric vertices
			for (Point3D p : positions) {
				out.write("v " + p.x + " " + p.y + " " + p.z);
				out.newLine();
			}

			// serialize the texture coordinates
			for (TexCoord2D t : texCoords) {
				out.write("vt " + t.u + " " + t.v);
				out.newLine();
			}

			// serialize the vertex normals
			for (Normal3D n : normals) {
				out.write("vn " + n.x + " " + n.y + " " + n.z);
				out.newLine();
			}

			// serialize the faces
			for (TriangularFace f : mesh.faces) {
				String[] vertexStrings = new String[3];

				for (int i = 0; i < 3; i++) {
					Vertex3D v = mesh.vertices.get(f.indices[i]);

					int pIndex = positions.indexOf(v.position) + 1;
					int tIndex = texCoords.indexOf(v.texCoord) + 1;
					int nIndex = normals.indexOf(v.normal) + 1;

					vertexStrings[i] = pIndex + "/" + tIndex + "/" + nIndex;
				}

				out.write("f " + vertexStrings[0] + " " + vertexStrings[1] + " " + vertexStrings[2]);
				out.newLine();
			}

			System.out.println();
		}
	}
}
