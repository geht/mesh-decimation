package structs;

import util.Util;

/**
 * This class models a texture coordinate in 2D-space.
 * 
 * @author Marwin Rieger 962450
 */
public class TexCoord2D {
	/**
	 * Represents the u-coordinate.
	 */
	public float u;
	/**
	 * Represents the v-coordinate.
	 */
	public float v;

	/**
	 * Constructs a new {@link TexCoord2D} with the given coordinates.
	 * 
	 * @param u
	 *            the u-coordinate.
	 * @param v
	 *            the v-coordinate.
	 */
	public TexCoord2D(float u, float v) {
		this.u = u;
		this.v = v;
	}

	/**
	 * Constructs an interpolated {@link TexCoord2D} between the two given
	 * {@link TexCoord2D texcoords}.
	 * 
	 * @param pi
	 *            first {@link TexCoord2D}.
	 * @param pj
	 *            second {@link TexCoord2D}.
	 */
	public TexCoord2D(TexCoord2D pi, TexCoord2D pj) {
		this.u = Util.lerp(pi.u, pj.u, 0.5f);
		this.v = Util.lerp(pi.v, pj.v, 0.5f);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(u);
		result = prime * result + Float.floatToIntBits(v);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TexCoord2D other = (TexCoord2D) obj;
		if (Float.floatToIntBits(u) != Float.floatToIntBits(other.u))
			return false;
		if (Float.floatToIntBits(v) != Float.floatToIntBits(other.v))
			return false;
		return true;
	}
}
