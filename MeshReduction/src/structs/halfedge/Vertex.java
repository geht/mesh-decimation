package structs.halfedge;

import structs.Point3D;

/**
 * This class models a vertex for use in the directed edge data structure.
 * 
 * @author Marwin Rieger 962450
 *
 */
public class Vertex {
	/**
	 * Represents the index of this particular {@link Vertex} inside the vector
	 * array of the {@link HalfedgeMesh3D}.
	 */
	public int index = -1;
	/**
	 * Reference to the {@link Point3D} of this vertex.
	 */
	public Point3D position;
	/**
	 * Reference to one of the halfedges emanating from this vertex.
	 */
	public Halfedge halfedge;

	/**
	 * Constructs a {@link Vertex} with the given {@link Point3D}.
	 * 
	 * @param p
	 *            position {@link Point3D}.
	 */
	public Vertex(Point3D p) {
		this.position = p;
	}

	/**
	 * Constructs an interpolated {@link Vertex} between the two given
	 * {@link Point3D positions}.
	 * 
	 * @param pi
	 *            first {@link Point3D}.
	 * @param pj
	 *            second {@link Point3D}.
	 */
	public Vertex(Point3D pi, Point3D pj) {
		this.position = new Point3D(pi, pj);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((position == null) ? 0 : position.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vertex other = (Vertex) obj;
		if (position == null) {
			if (other.position != null)
				return false;
		} else if (!position.equals(other.position))
			return false;
		return true;
	}
}
