package structs.halfedge;

/**
 * This class models an edge between to vertices. They are used as a key for a
 * hash map during construction of the connectivity information in the
 * {@link HalfedgeMesh3D} to determine whether an opposite {@link Halfedge} for
 * a certain edge has already been created.
 * 
 * @author Marwin Rieger 962450
 */
public class Edge {
	/**
	 * Represents the index of the first vertex.
	 */
	public int vertexIndex0;
	/**
	 * Represents the index of the second vertex.
	 */
	public int vertexIndex1;

	/**
	 * Constructs a new {@link Edge} between the two given vertices. Note that
	 * the order of the indices is important for the use in the hash map.
	 * 
	 * @param index0
	 * @param index1
	 */
	public Edge(int index0, int index1) {
		this.vertexIndex0 = index0;
		this.vertexIndex1 = index1;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + vertexIndex0;
		result = prime * result + vertexIndex1;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Edge other = (Edge) obj;
		if (vertexIndex0 != other.vertexIndex0)
			return false;
		if (vertexIndex1 != other.vertexIndex1)
			return false;
		return true;
	}
}
