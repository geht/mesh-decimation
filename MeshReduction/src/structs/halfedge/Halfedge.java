package structs.halfedge;

import java.util.PriorityQueue;

import structs.Normal3D;
import structs.TexCoord2D;

/**
 * This class models a halfedge for the use in the directed edge data structure.
 * It implements {@link Comparable} for the use in a {@link PriorityQueue}.
 * 
 * @author Marwin Rieger 962450
 *
 */
public class Halfedge implements Comparable<Halfedge> {
	/**
	 * Represents the index of this particular {@link Halfedge} inside the half
	 * edge array of the directed edge data structure.
	 */
	public int index = -1;
	/**
	 * Holds a reference to the {@link Vertex} at the end of this
	 * {@link Halfedge}. This should never be <code>null</code> for a valid
	 * {@link Halfedge} inside the data structure.
	 */
	public Vertex vertex;
	/**
	 * Holds a reference to the oppositely oriented adjacent {@link Halfedge} if
	 * there is any, otherwise this should be <code>null</code>.
	 */
	public Halfedge opposite;
	/**
	 * Holds a reference to the {@link TexCoord2D} relating to the
	 * {@link Vertex} at the end of this {@link Halfedge}.
	 */
	public TexCoord2D texCoord;
	/**
	 * Holds a reference to the {@link Normal3D} relating to the {@link Vertex}
	 * at the end of this {@link Halfedge}.
	 */
	public Normal3D normal;
	/**
	 * Holds information about whether this {@link Halfedge} has been oriented
	 * yet. This is needed to be able to construct the connectivity information
	 * of the {@link HalfedgeMesh3D} correctly. After constructing the
	 * {@link HalfedgeMesh3D} and orienting its faces this value should always
	 * be <code>true</code> for a valid {@link Halfedge} inside the data
	 * structure.
	 */
	public boolean oriented = false;
	/**
	 * Holds information about the cost of a collapse of this particular
	 * {@link Halfedge} if it is collapsable. See {@link HalfedgeMesh3D} for
	 * more information on when a {@link HalfedgeMesh3D} is collapsable and the
	 * cost function.
	 * 
	 * @see HalfedgeMesh3D
	 */
	public double cost = Double.MAX_VALUE;

	@Override
	public int compareTo(Halfedge o) {
		return Double.compare(this.cost, o.cost);
	}
}
