package structs.halfedge;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;

import structs.Normal3D;
import structs.Point3D;
import structs.TexCoord2D;
import structs.mesh3d.IFSMesh3D;
import structs.mesh3d.TriangularFace;
import structs.mesh3d.Vertex3D;

/**
 * 
 * This class models a mesh in 3D-space using the directed edge data structure
 * to store {@link Vertex vertices}, {@link Halfedge halfedges} and faces
 * (implicitly) and hold connectivity information between them.
 * 
 * @author Marwin Rieger 962450
 *
 */
public class HalfedgeMesh3D {
	private static final String NONMANIFOLD = "Half-Edge can't represent non-manifold meshes.";
	private static final String NONORIENTABLE = "This mesh can't be oriented unambiguously.";

	/**
	 * Represents the (file)name of this mesh/model.
	 */
	public String name = "";
	/**
	 * Holds the path to the obj-file of this mesh.
	 */
	public String path = "";

	/**
	 * Holds the {@link Vertex vertices} of this mesh.
	 */
	public Vertex[] vertices;
	/**
	 * Holds the ordered {@link Halfedge halfedges} and therefore implicitly the
	 * faces of this mesh.
	 * <p>
	 * The halfedges of a face can be accessed using the index of any
	 * {@link Halfedge} of this face like this:
	 * <p>
	 * <code>int f = index / 3; // face index</code><br>
	 * <code>int e = index % 3; // edge subindex</code><br>
	 * <code>// Halfedge a = halfedges[index] or;</code><br>
	 * <code>Halfedge a = halfedges[3 * f + (e + 0) % 3];</code><br>
	 * <code>Halfedge b = halfedges[3 * f + (e + 1) % 3]; // is a.next</code>
	 * <br>
	 * <code>Halfedge c = halfedges[3 * f + (e + 2) % 3]; // is a.prev</code>
	 * <br>
	 */
	public Halfedge[] halfedges;

	/**
	 * Represents the amount of faces this mesh currently has. The amount of
	 * faces is decreased when the mesh gets reduced/decimated.
	 */
	public int faceAmount = 0;

	/**
	 * Holds a {@link Halfedge} belonging to the edge between two {@link Vertex
	 * vertices} represented by an {@link Edge} as the key. This is used to
	 * create the opposite relations of the {@link Halfedge halfedges}.
	 */
	private HashMap<Edge, Halfedge> halfEdgeMap;
	/**
	 * Holds the indices of those {@link Halfedge halfedges} that have yet to be
	 * oriented during the construction of the connectivity information.
	 */
	private LinkedList<Integer> stack;
	/**
	 * Holds references to those {@link Halfedge halfedges} that can be
	 * collapsed during reduction/decimation.
	 * 
	 * @see #reduceTo(int)
	 * @see #findCollapsableEdges()
	 * @see #isCollapsable(int)
	 * @see #collapsEdge(int)
	 */
	private PriorityQueue<Halfedge> collapsableEdges;

	/**
	 * Constructs a {@link HalfedgeMesh3D} using an {@link IFSMesh3D} at its
	 * base.
	 * 
	 * @param mesh
	 *            {@link IFSMesh3D} to construct a {@link HalfedgeMesh3D} from.
	 */
	public HalfedgeMesh3D(IFSMesh3D mesh) {
		name = mesh.name;
		path = mesh.path;

		faceAmount = mesh.faces.size();
		halfedges = new Halfedge[faceAmount * 3];

		// first get all the vertices
		ArrayList<Vertex> vs = new ArrayList<>(faceAmount);
		for (int i = 0; i < mesh.vertices.size(); i++) {
			Vertex3D v3d = mesh.vertices.get(i);
			Vertex v = new Vertex(v3d.position);

			if (!vs.contains(v)) {
				int vi = vs.size();
				v.index = vi;
				vs.add(v);
			} else {
				// accessing the correct vertices when constructing the
				// halfedges/faces later on becomes way more convenient when
				// doing this
				vs.add(vs.get(vs.indexOf(v)));
			}
		}
		vertices = vs.toArray(new Vertex[vs.size()]);

		// oh boy... construct the halfedges now
		halfEdgeMap = new HashMap<>(mesh.faces.size() * 3);
		for (int i = 0; i < mesh.faces.size() * 3; i += 3) {
			// get the face
			TriangularFace tface = mesh.faces.get(i / 3);

			// create the halfedges the face will be made up of
			Halfedge a = new Halfedge();
			Halfedge b = new Halfedge();
			Halfedge c = new Halfedge();

			// assign the indices accordingly
			a.index = i + 0;
			b.index = i + 1;
			c.index = i + 2;

			// get the indices of the vertices of this face to be able to
			// determine which vertices the new halfedges will belong to later
			// on
			int[] indices = new int[3];
			indices[0] = vertices[tface.indices[0]].index;
			indices[1] = vertices[tface.indices[1]].index;
			indices[2] = vertices[tface.indices[2]].index;

			// construct the edges like this, because the order of the indices
			// matter for the hash map
			// this lets us know which halfedges belong to which edge and
			// therefore which halfedges have to be brothers/opposites
			Edge e0 = new Edge(Math.min(indices[0], indices[1]), Math.max(indices[0], indices[1]));
			Edge e1 = new Edge(Math.min(indices[1], indices[2]), Math.max(indices[1], indices[2]));
			Edge e2 = new Edge(Math.min(indices[2], indices[0]), Math.max(indices[2], indices[0]));

			// determine which halfedges have to be brothers/opposites by trying
			// to add them to the hash map using the keys specified above to
			// know which halfedge belongs to which edge
			// if there already is a halfedge in there they have to be opposites
			// and are connected accordingly
			// if there already is a halfedge in there that already has got an
			// opposite then the given mesh is not two-manifold and therefore
			// can't be represented by this data structure
			// if there is no halfedge in there just add it
			if (halfEdgeMap.containsKey(e0)) {
				Halfedge brother = halfEdgeMap.get(e0);

				if (brother.opposite != null)
					throw new IllegalArgumentException(NONMANIFOLD);

				a.opposite = brother;
				brother.opposite = a;
			} else
				halfEdgeMap.put(e0, a);
			if (halfEdgeMap.containsKey(e1)) {
				Halfedge brother = halfEdgeMap.get(e1);

				if (brother.opposite != null)
					throw new IllegalArgumentException(NONMANIFOLD);

				b.opposite = brother;
				brother.opposite = b;
			} else
				halfEdgeMap.put(e1, b);
			if (halfEdgeMap.containsKey(e2)) {
				Halfedge brother = halfEdgeMap.get(e2);

				if (brother.opposite != null)
					throw new IllegalArgumentException(NONMANIFOLD);

				c.opposite = brother;
				brother.opposite = c;
			} else
				halfEdgeMap.put(e2, c);

			// add the halfedges to our data structure
			// the next and previous connections are implicitly given by the
			// data structure and not correct yet
			// the faces will be oriented next
			halfedges[a.index] = a;
			halfedges[b.index] = b;
			halfedges[c.index] = c;
		}

		// orient the halfedges in the data structure correctly
		stack = new LinkedList<>();
		for (int i = 0; i < halfedges.length; i += 3) {
			// get the halfedges of this face
			Halfedge a = halfedges[i + 0];
			Halfedge b = halfedges[i + 1];
			Halfedge c = halfedges[i + 2];

			// if all of the halfedges have been oriented everything's fine
			// if they haven't then orient them
			// if only some of them have been oriented then this mesh can't be
			// oriented unambiguously (imagine a möbius strip)
			if (a.oriented && b.oriented && c.oriented) {
				continue;
			} else if (!a.oriented && !b.oriented && !c.oriented) {
				// get the face of the old mesh and their vertices
				int faceIndex = i / 3;
				TriangularFace tface = mesh.faces.get(faceIndex);
				Vertex3D v3d0 = mesh.vertices.get(tface.indices[0]);
				Vertex3D v3d1 = mesh.vertices.get(tface.indices[1]);
				Vertex3D v3d2 = mesh.vertices.get(tface.indices[2]);

				// get the new vertices as well
				Vertex v0 = vertices[tface.indices[0]];
				Vertex v1 = vertices[tface.indices[1]];
				Vertex v2 = vertices[tface.indices[2]];

				// orient this face
				// in this case the orientation can be arbitrary as long as the
				// adjacent face get oriented accordingly
				orient(v3d0, v3d1, v3d2, v0, v1, v2, a, b, c);

				// add the adjacent faces to the stack so they can be oriented
				// next
				if (a.opposite != null)
					stack.push(a.opposite.index);
				if (b.opposite != null)
					stack.push(b.opposite.index);
				if (c.opposite != null)
					stack.push(c.opposite.index);

				// get the faces from the stack and orient them so that the
				// direction of the halfedges are correct
				while (!stack.isEmpty()) {
					int halfEdgeIndex = stack.pop();
					orientFace(halfEdgeIndex, mesh);
				}
			} else {
				throw new IllegalArgumentException(NONORIENTABLE);
			}
		}
	}

	/**
	 * Orients the face represented by the given halfedge so that it's direction
	 * is oppositely from it's opposite. May sound weird expressed like that but
	 * has to be done so that each face is oriented correctly.
	 * 
	 * @param halfEdgeIndex
	 *            index of the {@link Halfedge} to orient its face.
	 * @param mesh
	 *            old mesh to get the vertex information from.
	 */
	private void orientFace(int halfEdgeIndex, IFSMesh3D mesh) {
		int faceIndex = halfEdgeIndex / 3;
		int subIndex = halfEdgeIndex % 3;
		// get the face of the old mesh
		TriangularFace face = mesh.faces.get(faceIndex);

		// get the halfedges
		Halfedge x = halfedges[halfEdgeIndex];
		Halfedge y;
		Halfedge z;

		// temporary halfedges for checks
		Halfedge t1 = halfedges[3 * faceIndex + (subIndex + 1) % 3];
		Halfedge t2 = halfedges[3 * faceIndex + (subIndex + 2) % 3];

		// temporary vertices for checks
		Vertex vt0 = vertices[face.indices[0]];
		Vertex vt1 = vertices[face.indices[1]];
		Vertex vt2 = vertices[face.indices[2]];

		// determine which vertex has to be which
		Vertex v0 = halfedges[3 * (x.opposite.index / 3) + (x.opposite.index + 2) % 3].vertex;
		Vertex v1 = x.opposite.vertex;
		Vertex v2 = vt0 != v0 && vt0 != v1 ? vt0 : vt1 != v0 && vt1 != v1 ? vt1 : vt2 != v0 && vt2 != v1 ? vt2 : null;

		// get the edges between the other two vertices to be able to get their
		// halfedges to be able to determine which halfedge has to be which
		Edge e1 = new Edge(Math.min(v2.index, v0.index), Math.max(v2.index, v0.index));
		Edge e2 = new Edge(Math.min(v1.index, v2.index), Math.max(v1.index, v2.index));

		// get the halfedges from the map (will be null if nothing's in there)
		Halfedge yo = halfEdgeMap.get(e1);
		Halfedge zo = halfEdgeMap.get(e2);

		// determine which halfedge has to be which
		y = t1 == yo || t1 == yo.opposite ? t1 : t2 == yo || t2 == yo.opposite ? t2 : null;
		z = t1 == zo || t1 == zo.opposite ? t1 : t2 == zo || t2 == zo.opposite ? t2 : null;

		// get the vertices of the old mesh
		Vertex3D v3dt0 = mesh.vertices.get(face.indices[0]);
		Vertex3D v3dt1 = mesh.vertices.get(face.indices[1]);
		Vertex3D v3dt2 = mesh.vertices.get(face.indices[2]);

		// determine which vertex has to be which
		Vertex3D v3d0 = v3dt0.position.equals(v0.position) ? v3dt0
				: v3dt1.position.equals(v0.position) ? v3dt1 : v3dt2.position.equals(v0.position) ? v3dt2 : null;
		Vertex3D v3d1 = v3dt0.position.equals(v1.position) ? v3dt0
				: v3dt1.position.equals(v1.position) ? v3dt1 : v3dt2.position.equals(v1.position) ? v3dt2 : null;
		Vertex3D v3d2 = v3dt0.position.equals(v2.position) ? v3dt0
				: v3dt1.position.equals(v2.position) ? v3dt1 : v3dt2.position.equals(v2.position) ? v3dt2 : null;

		// if this face is already oriented then check if the orientation is
		// correct
		// if it isn't or only some halfedges of this face are oriented then
		// this mesh is can't be oriented unambiguously
		// if it isn't oriented then orient it
		if (x.oriented && y.oriented && z.oriented) {
			if (x.vertex != v0 || y.vertex != v2 || z.vertex != v1)
				throw new IllegalArgumentException(NONORIENTABLE);
			else
				return;
		} else if (!x.oriented && !y.oriented && !z.oriented) {
			// orient this face so that its orientation is oppositely from the
			// opposite (x.opposite) face
			orient(v3d1, v3d0, v3d2, v1, v0, v2, x, y, z);

			// add the faces that haven't been oriented yet to the stack so they
			// will be oriented
			// x.opposite has already been oriented (since "we came" from that
			// face) so it doesn't have to be oriented again.
			if (y.opposite != null)
				stack.push(y.opposite.index);
			if (z.opposite != null)
				stack.push(z.opposite.index);
		} else {
			throw new IllegalArgumentException(NONORIENTABLE);
		}
	}

	/**
	 * 
	 * Creates the connections between the {@link Halfedge halfedges} and
	 * {@link Vertex vertices} as well as the prev and next relations. The
	 * {@link Vertex3D vertices} from the original face are needed to set the
	 * {@link TexCoord2D texcoords} and {@link Normal3D normals} in the
	 * {@link Halfedge halfedges} accordingly.
	 * 
	 * @param v3d0
	 *            {@link Vertex3D} v0 of the original face.
	 * @param v3d1
	 *            {@link Vertex3D} v1 of the original face.
	 * @param v3d2
	 *            {@link Vertex3D} v2 of the original face.
	 * @param v0
	 *            {@link Vertex} v0 of the face.
	 * @param v1
	 *            {@link Vertex} v1 of the face.
	 * @param v2
	 *            {@link Vertex} v2 of the face.
	 * @param a
	 *            {@link Halfedge} a of the face.
	 * @param b
	 *            {@link Halfedge} b of the face.
	 * @param c
	 *            {@link Halfedge} c of the face.
	 */
	private void orient(Vertex3D v3d0, Vertex3D v3d1, Vertex3D v3d2, Vertex v0, Vertex v1, Vertex v2, Halfedge a,
			Halfedge b, Halfedge c) {
		// get face- and subindex
		int faceIndex = a.index / 3;
		int subIndex = a.index % 3;

		// set the vertex references in the halfedges
		a.vertex = v1;
		b.vertex = v2;
		c.vertex = v0;

		// set the halfedge references in the vertices
		v0.halfedge = a;
		v1.halfedge = b;
		v2.halfedge = c;

		// set the texcoord references in the halfedges
		a.texCoord = v3d1.texCoord;
		b.texCoord = v3d2.texCoord;
		c.texCoord = v3d0.texCoord;

		// set the normal references in the halfedges
		a.normal = v3d1.normal;
		b.normal = v3d2.normal;
		c.normal = v3d0.normal;

		// set the halfedges to oriented
		a.oriented = true;
		b.oriented = true;
		c.oriented = true;

		// update the indices of the halfedges
		// this determines their prev and next relations
		a.index = 3 * faceIndex + (subIndex + 0) % 3;
		b.index = 3 * faceIndex + (subIndex + 1) % 3;
		c.index = 3 * faceIndex + (subIndex + 2) % 3;

		// update the position of the halfedges inside the data structure
		// this ensures that their prev and next relations are correct
		halfedges[a.index] = a;
		halfedges[b.index] = b;
		halfedges[c.index] = c;
	}

	/**
	 * Iteratively reduces the amount of faces of this mesh to the given desired
	 * amount of faces.
	 * 
	 * @param desiredAmount
	 *            desired amount of faces.
	 * 
	 * @see #findCollapsableEdges()
	 * @see #collapsEdge(int)
	 */
	public void reduceTo(int desiredAmount) {
		findCollapsableEdges();
		while (faceAmount > desiredAmount && !collapsableEdges.isEmpty()) {
			System.out.println(faceAmount + " faces");
			collapsEdge(collapsableEdges.poll().index);
			findCollapsableEdges();
		}
	}

	/**
	 * Finds collapsable halfedges.
	 * 
	 * @see #isCollapsable(int)
	 * @see #collapseCost(int)
	 */
	private void findCollapsableEdges() {
		collapsableEdges = new PriorityQueue<>();

		for (int i = 0; i < halfedges.length; i++) {
			Halfedge he = halfedges[i];

			if (he == null)
				continue;

			if (isCollapsable(i)) {
				he.cost = collapseCost(i);
				collapsableEdges.add(he);
			}
		}
	}

	/**
	 * 
	 * Determines for the {@link Halfedge} at the given index whether it is
	 * collapsable or not. <br>
	 * The following conditions are checked:
	 * <li>Each {@link Vertex} pk which is adjacent to the edge (not only
	 * halfedge!) has to have a valence greater than 3.</li>
	 * <li>If both of vertices of the edge are border vertices then the edge has
	 * to be a border edge.</li>
	 * <p>
	 * If these conditions are met then the edge is collapsable. If not then the
	 * edge is not collapsable.
	 * <p>
	 * Note: There are more conditions (given in the lecture) to this, which all
	 * seemed like tautologies which is why they aren't checked here anymore.
	 * They used to be checked as well, but it seemed to make no difference.
	 * Nevertheless other criteria could be added as well.
	 * 
	 * @param halfEdgeIndex
	 *            index of the {@link Halfedge} which is to be checked.
	 * @return <code>true</code> if the {@link Halfedge} at the given index is
	 *         collapsable, otherwise <code>false</code>.
	 */
	private boolean isCollapsable(int halfEdgeIndex) {
		// get face- and subindex
		int f = halfEdgeIndex / 3;
		int e = halfEdgeIndex % 3;

		// get the halfedges of this face
		Halfedge a = halfedges[3 * f + (e + 0) % 3];
		Halfedge b = halfedges[3 * f + (e + 1) % 3];
		Halfedge c = halfedges[3 * f + (e + 2) % 3];

		// get the vertices of this face
		Vertex pi = c.vertex;
		Vertex pj = a.vertex;
		Vertex pk = b.vertex;

		// valence check
		if (getValence(pk) <= 3)
			return false;

		// get the vertex pk on the other side if there is one
		pk = a.opposite != null ? halfedges[3 * (a.opposite.index / 3) + (a.opposite.index + 1) % 3].vertex : null;

		// valence check
		// will behave correctly even if pk is null
		if (getValence(pk) <= 3)
			return false;

		// check whether the edge is a border edge when both vertices are border
		// vertices
		if (isBorderVertex(pi) && isBorderVertex(pj) && a.opposite != null)
			return false;

		// everything checked out
		return true;
	}

	/**
	 * Edgecollapses the edge specified by the index of a collapsable
	 * {@link Halfedge}. This means the new {@link Vertex} will be an
	 * interpolation between the old vertices. This operation reduces the amount
	 * of faces by at least one face and by two faces at most (depending on
	 * whether this is a border ege or not). The {@link Halfedge halfedges},
	 * {@link Vertex vertices} and relations between them and each other are
	 * updated accordingly.
	 * <p>
	 * Note: I wasn't entirely sure what to do with the texcoords if there are
	 * any. I'm even more clueless on what to do with the normals if there are
	 * any. So for now texcoords are being interpolated between and normals are
	 * ignored.
	 * 
	 * @param collapsableEdgeIndex
	 *            index of a {@link Halfedge} which is collapsable.
	 * 
	 * @see #isCollapsable(int)
	 */
	private void collapsEdge(int collapsableEdgeIndex) {
		// get face- and subindex
		int f = collapsableEdgeIndex / 3;
		int e = collapsableEdgeIndex % 3;

		// get the halfedges of this face
		Halfedge a = halfedges[3 * f + (e + 0) % 3];
		Halfedge b = halfedges[3 * f + (e + 1) % 3];
		Halfedge c = halfedges[3 * f + (e + 2) % 3];

		// get the vertices of this face
		Vertex pi = c.vertex;
		Vertex pj = a.vertex;
		Vertex pk = b.vertex;

		// get all incoming halfedges aka the halfedges which have to be updated
		List<Integer> incoming = getIncomingHalfEdges(pi);
		incoming.addAll(getIncomingHalfEdges(pj));

		// construct the new vertex
		// set its halfedge and index accordingly
		Vertex p = new Vertex(pi.position, pj.position);
		p.halfedge = c.opposite != null ? c.opposite
				: halfedges[3 * (b.opposite.index / 3) + (b.opposite.index + 1) % 3];
		p.index = Math.min(pi.index, pj.index);

		// update the halfedge reference of pk
		pk.halfedge = b.opposite != null ? b.opposite
				: halfedges[3 * (c.opposite.index / 3) + (c.opposite.index + 1) % 3];

		// update all incoming edges
		for (Integer hei : incoming) {
			Halfedge he = halfedges[hei];
			he.vertex = p;
			he.texCoord = new TexCoord2D(c.texCoord, a.texCoord);
		}

		// if there is a face on the other side it has to be deleted too
		if (a.opposite != null) {
			// get face- and subindex
			int of = a.opposite.index / 3;
			int oe = a.opposite.index % 3;

			// get the halfedges of this face
			Halfedge x = halfedges[3 * of + (oe + 0) % 3];
			Halfedge y = halfedges[3 * of + (oe + 1) % 3];
			Halfedge z = halfedges[3 * of + (oe + 2) % 3];

			// get the vertex pk which has to be updated
			// update the halfedge reference of pk
			Vertex pk1 = y.vertex;
			pk1.halfedge = y.opposite != null ? y.opposite
					: halfedges[3 * (z.opposite.index / 3) + (z.opposite.index + 1) % 3];

			// update all incoming edges
			for (Integer hei : incoming) {
				Halfedge he = halfedges[hei];
				he.texCoord = new TexCoord2D(new TexCoord2D(c.texCoord, a.texCoord),
						new TexCoord2D(x.texCoord, z.texCoord));
			}

			// update the opposite relations
			if (y.opposite != null)
				y.opposite.opposite = z.opposite;
			if (z.opposite != null)
				z.opposite.opposite = y.opposite;

			// update the data structure
			halfedges[x.index] = null;
			halfedges[y.index] = null;
			halfedges[z.index] = null;
			faceAmount--;
		}

		// update the opposite relations
		if (b.opposite != null)
			b.opposite.opposite = c.opposite;
		if (c.opposite != null)
			c.opposite.opposite = b.opposite;

		// update the data structure
		halfedges[a.index] = null;
		halfedges[b.index] = null;
		halfedges[c.index] = null;
		faceAmount--;

		vertices[pi.index] = p;
		vertices[pj.index] = p;
	}

	/**
	 * Determines whether the given {@link Vertex} is a border vertex or not by
	 * iterating over its outgoing {@link Halfedge halfedges}. If the iteration
	 * can complete a circle then the vertex can't be at a border. If it can't
	 * than it has to be at a border.
	 * 
	 * @param vertex
	 *            {@link Vertex} to be checked.
	 * @return <code>true</code> if the {@link Vertex} is a border vertex,
	 *         otherwise <code>false</code>.
	 */
	private boolean isBorderVertex(Vertex vertex) {
		int f;
		int e;
		Halfedge i = vertex.halfedge;
		do {
			if (i.opposite == null)
				return true;
			f = i.opposite.index / 3;
			e = i.opposite.index % 3;
			i = halfedges[3 * f + (e + 1) % 3];
		} while (i != vertex.halfedge);

		return false;
	}

	/**
	 * Calculates the valence of the given {@link Vertex} by counting its
	 * outgoing {@link Halfedge halfedges}. This method will return
	 * {@link Integer#MAX_VALUE} if the given {@link Vertex} is
	 * <code>null</code>.
	 * 
	 * @param vertex
	 *            {@link Vertex} to get valence from.
	 * @return valence of the given vertex or {@link Integer#MAX_VALUE}.
	 * 
	 * @see #getOutgoingHalfEdges(Vertex)
	 */
	private int getValence(Vertex vertex) {
		if (vertex == null)
			return Integer.MAX_VALUE;

		return getOutgoingHalfEdges(vertex).size();
	}

	/**
	 * Iterates over all outgoing {@link Halfedge halfedges} of the given
	 * {@link Vertex} and returns a list of them.
	 * 
	 * @param vertex
	 *            {@link Vertex} to get outgoing {@link Halfedge halfedges}
	 *            from.
	 * @return {@link List} of all outgoing {@link Halfedge halfedges}.
	 */
	private List<Integer> getOutgoingHalfEdges(Vertex vertex) {
		ArrayList<Integer> outgoing = new ArrayList<>();

		int f;
		int e;
		boolean border = false;
		Halfedge he = vertex.halfedge;
		do {
			outgoing.add(he.index);
			if (he.opposite == null) {
				border = true;
				break;
			}
			f = he.opposite.index / 3;
			e = he.opposite.index % 3;
			he = halfedges[3 * f + (e + 1) % 3];
		} while (he != vertex.halfedge);

		// loop around the other direction if necessary
		if (border) {
			f = vertex.halfedge.index / 3;
			e = vertex.halfedge.index % 3;

			Halfedge beginning = halfedges[3 * f + (e + 2) % 3].opposite;
			he = beginning;
			do {
				if (he == null)
					break;
				outgoing.add(he.index);
				f = he.index / 3;
				e = he.index % 3;
				he = halfedges[3 * f + (e + 2) % 3].opposite;
			} while (he != beginning);
		}

		return outgoing;
	}

	/**
	 * Iterates over all outgoing {@link Halfedge halfedges} of the given
	 * {@link Vertex}, collects all incoming {@link Halfedge halfedges} and
	 * returns a list of them.
	 * 
	 * @param vertex
	 *            {@link Vertex} to get incoming {@link Halfedge halfedges}
	 *            from.
	 * @return {@link List} of all incoming {@link Halfedge halfedges}.
	 */
	private List<Integer> getIncomingHalfEdges(Vertex vertex) {
		ArrayList<Integer> incoming = new ArrayList<>();

		int f;
		int e;
		boolean border = false;
		Halfedge he = vertex.halfedge;
		do {
			f = he.index / 3;
			e = he.index % 3;
			incoming.add(3 * f + (e + 2) % 3);
			if (he.opposite == null) {
				border = true;
				break;
			}
			f = he.opposite.index / 3;
			e = he.opposite.index % 3;
			he = halfedges[3 * f + (e + 1) % 3];
		} while (he != vertex.halfedge);

		// loop around the other direction if necessary
		if (border) {
			f = vertex.halfedge.index / 3;
			e = vertex.halfedge.index % 3;

			Halfedge beginning = halfedges[3 * f + (e + 2) % 3].opposite;
			he = beginning;
			do {
				if (he == null)
					break;
				f = he.index / 3;
				e = he.index % 3;
				incoming.add(3 * f + (e + 2) % 3);
				he = halfedges[3 * f + (e + 2) % 3].opposite;
			} while (he != beginning);
		}

		return incoming;
	}

	/**
	 * Implementation of a (or the) simple cost function (from the lecture) for
	 * an edge collapse.
	 * 
	 * @param halfEdgeIndex
	 *            index of the (collapsable) edge to calculate the costs from.
	 * @return cost of the edge collapse.
	 */
	private double collapseCost(int halfEdgeIndex) {
		double cost;

		// get face- and subindex
		int fi = halfEdgeIndex / 3;
		int e = halfEdgeIndex % 3;

		// get the relevant halfedges
		Halfedge a = halfedges[3 * fi + (e + 0) % 3];
		Halfedge c = halfedges[3 * fi + (e + 2) % 3];

		// get the vertices of this edge
		Vertex pi = c.vertex;
		Vertex pj = a.vertex;

		// get pi - pj
		Point3D diff = new Point3D(pi.position.x - pj.position.x, pi.position.y - pj.position.y,
				pi.position.z - pj.position.z);

		// get len(pi - pj)
		double len = Math.sqrt(diff.x * diff.x + diff.y * diff.y + diff.z * diff.z);

		// get the max of the min values
		double max = Double.MIN_VALUE;
		for (Integer f : getOutgoingHalfEdges(pi)) {
			// get the normals of the faces
			Normal3D fn = getNormal(f);
			Normal3D gn = getNormal(a.index);

			// calculate the value
			double val0 = (1.0 - (fn.x * gn.x + fn.y * gn.y + fn.z * gn.z)) / 2.0;

			// if there is an opposite then we have to get the min of both
			// values
			// update max value
			if (a.opposite != null) {
				// get the normal of the face
				gn = getNormal(a.opposite.index);

				// calculate the value
				double val1 = (1.0 - (fn.x * gn.x + fn.y * gn.y + fn.z * gn.z)) / 2.0;

				max = Math.max(max, Math.min(val0, val1));
			} else {
				max = Math.max(max, val0);
			}
		}

		// cost(pi, pk) = len(pi - pj) * max(min((1 - dot(f.n, g.n)) / 2))
		cost = len * max;

		return cost;
	}

	/**
	 * Calculates the face normal of the face of the {@link Halfedge} at the
	 * given index.
	 * 
	 * @param halfEdgeIndex
	 *            index of a {@link Halfedge}.
	 * @return face normal.
	 */
	private Normal3D getNormal(int halfEdgeIndex) {
		// get face- and subindex
		int f = halfEdgeIndex / 3;
		int e = halfEdgeIndex % 3;

		// get the halfedges of this face
		Halfedge a = halfedges[3 * f + (e + 0) % 3];
		Halfedge b = halfedges[3 * f + (e + 1) % 3];
		Halfedge c = halfedges[3 * f + (e + 2) % 3];

		// get the positions of the vertices of this face
		Point3D p1 = a.vertex.position;
		Point3D p2 = b.vertex.position;
		Point3D p3 = c.vertex.position;

		// get the vectors u and v
		// u = p2 - p1
		// v = p3 - p1
		Point3D u = new Point3D(p2.x - p1.x, p2.y - p1.y, p2.z - p1.z);
		Point3D v = new Point3D(p3.x - p1.x, p3.y - p1.y, p3.z - p1.z);

		// cross product of u and v is the normal
		float nx = u.y * v.z - u.z * v.y;
		float ny = u.z * v.x - u.x * v.z;
		float nz = u.x * v.y - u.y * v.x;

		// get length of the new vector
		float len = (float) Math.sqrt(nx * nx + ny * ny + nz * nz);

		// return normalized vector
		return new Normal3D(nx / len, ny / len, nz / len);
	}

}
