package structs;

/**
 * This class models a vertex normal in 3D-space.
 * 
 * @author Marwin Rieger 962450
 */
public class Normal3D {
	/**
	 * Represents the x-coordinate.
	 */
	public float x;
	/**
	 * Represents the y-coordinate.
	 */
	public float y;
	/**
	 * Represents the z-coordinate.
	 */
	public float z;

	/**
	 * Constructs a new {@link Normal3D} with the given coordinates.
	 * 
	 * @param x
	 *            the x-coordinate.
	 * @param y
	 *            the y-coordinate.
	 * @param z
	 *            the z-coordinate.
	 */
	public Normal3D(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(x);
		result = prime * result + Float.floatToIntBits(y);
		result = prime * result + Float.floatToIntBits(z);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Normal3D other = (Normal3D) obj;
		if (Float.floatToIntBits(x) != Float.floatToIntBits(other.x))
			return false;
		if (Float.floatToIntBits(y) != Float.floatToIntBits(other.y))
			return false;
		if (Float.floatToIntBits(z) != Float.floatToIntBits(other.z))
			return false;
		return true;
	}
}
