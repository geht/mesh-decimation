package structs;

import util.Util;

/**
 * This class models a point in 3D-space.
 * 
 * @author Marwin Rieger 962450
 */
public class Point3D {
	/**
	 * Represents the x-coordinate.
	 */
	public float x;
	/**
	 * Represents the y-coordinate.
	 */
	public float y;
	/**
	 * Represents the z-coordinate.
	 */
	public float z;

	/**
	 * Constructs a new {@link Point3D} with the given coordinates.
	 * 
	 * @param x
	 *            the x-coordinate.
	 * @param y
	 *            the y-coordinate.
	 * @param z
	 *            the z-coordinate.
	 */
	public Point3D(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	/**
	 * Constructs an interpolated {@link Point3D} between the two given
	 * {@link Point3D}s.
	 * 
	 * @param pi
	 *            first {@link Point3D}.
	 * @param pj
	 *            second {@link Point3D}.
	 */
	public Point3D(Point3D pi, Point3D pj) {
		this.x = Util.lerp(pi.x, pj.x, 0.5f);
		this.y = Util.lerp(pi.y, pj.y, 0.5f);
		this.z = Util.lerp(pi.z, pj.z, 0.5f);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(x);
		result = prime * result + Float.floatToIntBits(y);
		result = prime * result + Float.floatToIntBits(z);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Point3D other = (Point3D) obj;
		if (Float.floatToIntBits(x) != Float.floatToIntBits(other.x))
			return false;
		if (Float.floatToIntBits(y) != Float.floatToIntBits(other.y))
			return false;
		if (Float.floatToIntBits(z) != Float.floatToIntBits(other.z))
			return false;
		return true;
	}
}
