package structs.mesh3d;

import java.util.ArrayList;
import java.util.List;

import structs.halfedge.Halfedge;
import structs.halfedge.HalfedgeMesh3D;

/**
 * 
 * This class models a mesh in 3D-space based on an indexed face set data
 * structure. This mesh holds no special connectivity information whatsoever.
 * Just plain simple vertices and faces which could be easily converted to VBO.
 * 
 * @author Marwin Rieger 962450
 *
 */
public class IFSMesh3D {
	/**
	 * Represents the (file)name of this mesh/model.
	 */
	public String name;
	/**
	 * Holds the path to the obj-file of this mesh.
	 */
	public String path;

	/**
	 * Holds the {@link Vertex3D vertices} of this mesh.
	 */
	public List<Vertex3D> vertices;
	/**
	 * Holds the {@link TriangularFace faces} of this mesh.
	 */
	public List<TriangularFace> faces;

	/**
	 * Constructs an {@link IFSMesh3D} from the given parameters.
	 * 
	 * @param name
	 *            (file)name of this mesh/model.
	 * @param path
	 *            path to the obj-file of this mesh.
	 * @param vertices
	 *            {@link List} of {@link Vertex3D vertices}.
	 * @param faces
	 *            {@link List} of {@link TriangularFace faces}.
	 */
	public IFSMesh3D(String name, String path, List<Vertex3D> vertices, List<TriangularFace> faces) {
		this.name = name;
		this.path = path;
		this.vertices = vertices;
		this.faces = faces;
	}

	/**
	 * Constructs an {@link IFSMesh3D} from the given {@link HalfedgeMesh3D}.
	 * 
	 * @param mesh
	 *            {@link HalfedgeMesh3D} to construct {@link IFSMesh3D} from.
	 */
	public IFSMesh3D(HalfedgeMesh3D mesh) {
		name = mesh.name;
		path = mesh.path;
		vertices = new ArrayList<>(mesh.vertices.length);
		faces = new ArrayList<>(mesh.halfedges.length / 3);

		for (int i = 0; i < mesh.halfedges.length; i += 3) {
			// get the halfedges of the face
			Halfedge a = mesh.halfedges[i + 0];
			Halfedge b = mesh.halfedges[i + 1];
			Halfedge c = mesh.halfedges[i + 2];

			// if a == null the face has been deleted
			if (a == null)
				continue;

			// construct the vertices from the halfedges
			Vertex3D v0 = new Vertex3D(c.vertex.position, c.texCoord, c.normal);
			Vertex3D v1 = new Vertex3D(a.vertex.position, a.texCoord, a.normal);
			Vertex3D v2 = new Vertex3D(b.vertex.position, b.texCoord, b.normal);

			int ai;
			int bi;
			int ci;

			// add vertices to the vertices list
			// get the indices of the vertices
			if (!vertices.contains(v0)) {
				ai = vertices.size();
				vertices.add(v0);
			} else {
				ai = vertices.indexOf(v0);
			}

			if (!vertices.contains(v1)) {
				bi = vertices.size();
				vertices.add(v1);
			} else {
				bi = vertices.indexOf(v1);
			}

			if (!vertices.contains(v2)) {
				ci = vertices.size();
				vertices.add(v2);
			} else {
				ci = vertices.indexOf(v2);
			}

			// add face to the faces list
			faces.add(new TriangularFace(ai, bi, ci));
		}
	}
}
