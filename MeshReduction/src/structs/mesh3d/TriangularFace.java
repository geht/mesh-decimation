package structs.mesh3d;

import java.util.Arrays;

/**
 * This class models a triangular face for use in the indexed face set data
 * structure. Each {@link TriangularFace} holds three indices of {@link Vertex3D
 * vertices}.
 * 
 * @author Marwin Rieger 962450
 *
 */
public class TriangularFace {
	/**
	 * Holds the three indices of the {@link Vertex3D vertices}.
	 */
	public int[] indices;

	/**
	 * Constructs a {@link TriangularFace} with the given indices of the
	 * {@link Vertex3D vertices}.
	 *
	 * @param a
	 *            index of {@link Vertex3D} v0
	 * @param b
	 *            index of {@link Vertex3D} v1
	 * @param c
	 *            index of {@link Vertex3D} v2
	 */
	public TriangularFace(int a, int b, int c) {
		indices = new int[3];
		indices[0] = a;
		indices[1] = b;
		indices[2] = c;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(indices);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TriangularFace other = (TriangularFace) obj;
		if (!Arrays.equals(indices, other.indices))
			return false;
		return true;
	}
}
