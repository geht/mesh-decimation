package structs.mesh3d;

import structs.Normal3D;
import structs.Point3D;
import structs.TexCoord2D;

/**
 * This class models a vertex for use in the indexed face set data structure.
 * 
 * @author Marwin Rieger 962450
 */
public class Vertex3D {
	/**
	 * Reference to the {@link Point3D} of this vertex.
	 */
	public Point3D position;
	/**
	 * Reference to the {@link TexCoord2D} of this vertex.
	 */
	public TexCoord2D texCoord;
	/**
	 * Reference to the {@link Normal3D} of this vertex.
	 */
	public Normal3D normal;

	/**
	 * Constructs a {@link Vertex3D} from the given parameters.
	 * 
	 * @param px
	 *            x-coordinate of the vertex.
	 * @param py
	 *            y-coordinate of the vertex.
	 * @param pz
	 *            z-coordinate of the vertex.
	 * @param u
	 *            u-coordinate of the vertex texture coordinate.
	 * @param v
	 *            u-coordinate of the vertex texture coordinate.
	 * @param nx
	 *            x-coordinate of the vertex normal.
	 * @param ny
	 *            y-coordinate of the vertex normal.
	 * @param nz
	 *            z-coordinate of the vertex normal.
	 */
	public Vertex3D(float px, float py, float pz, float u, float v, float nx, float ny, float nz) {
		this.position = new Point3D(px, py, pz);
		this.texCoord = new TexCoord2D(u, v);
		this.normal = new Normal3D(nx, ny, nz);
	}

	/**
	 * Constructs a {@link Vertex3D} from the given parameters.
	 * 
	 * @param p
	 *            {@link Point3D} of the vertex.
	 * @param t
	 *            {@link TexCoord2D} of the vertex.
	 * @param n
	 *            {@link Normal3D} of the vertex.
	 */
	public Vertex3D(Point3D p, TexCoord2D t, Normal3D n) {
		this.position = p;
		this.texCoord = t;
		this.normal = n;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((normal == null) ? 0 : normal.hashCode());
		result = prime * result + ((position == null) ? 0 : position.hashCode());
		result = prime * result + ((texCoord == null) ? 0 : texCoord.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vertex3D other = (Vertex3D) obj;
		if (normal == null) {
			if (other.normal != null)
				return false;
		} else if (!normal.equals(other.normal))
			return false;
		if (position == null) {
			if (other.position != null)
				return false;
		} else if (!position.equals(other.position))
			return false;
		if (texCoord == null) {
			if (other.texCoord != null)
				return false;
		} else if (!texCoord.equals(other.texCoord))
			return false;
		return true;
	}
}
