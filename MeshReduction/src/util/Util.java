package util;

/**
 * This class holds helper methods.
 * 
 * @author Marwin Rieger 962450
 *
 */
public class Util {
	/**
	 * Implementation of linear interpolation between two values.
	 * 
	 * @param a
	 *            first value.
	 * @param b
	 *            second value.
	 * @param m
	 *            interpolation factor.
	 * @return interpolated value between a and b.
	 */
	public static float lerp(float a, float b, float m) {
		return (1.0f - m) * a + (m) * b;
	}
}
