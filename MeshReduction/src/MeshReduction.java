import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import parser.OBJParser;
import structs.halfedge.HalfedgeMesh3D;
import structs.mesh3d.IFSMesh3D;

/**
 * 
 * This class is the main entry point of the mesh reduction/simplification
 * application. If given valid parameters this program parses the specified
 * obj-file using {@link OBJParser#parseMesh3D(String)}, which constructs an
 * {@link IFSMesh3D}. This will then be converted to a {@link HalfedgeMesh3D}
 * with connectivity information. After that
 * {@link HalfedgeMesh3D#reduceTo(int)} is called (with the specified amount of
 * desired faces), which reduces/simplifies the mesh to the given amount of
 * faces as close as possible (sometimes it's one face less than specified).
 * Finally the reduced mesh is serialized using
 * {@link OBJParser#serializeMesh3D(IFSMesh3D, String)}.
 * 
 * @author Marwin Rieger 962450
 *
 */
public class MeshReduction {

	/**
	 * 
	 * Parses the specified obj-file using {@link OBJParser#parseMesh3D(String)}
	 * , which constructs an {@link IFSMesh3D}. This will then be converted to a
	 * {@link HalfedgeMesh3D} with connectivity information. After that
	 * {@link HalfedgeMesh3D#reduceTo(int)} is called (with the specified amount
	 * of desired faces), which reduces/simplifies the mesh to the given amount
	 * of faces as close as possible (sometimes it's one face less than
	 * specified). Finally the reduced mesh is serialized using
	 * {@link OBJParser#serializeMesh3D(IFSMesh3D, String)}.
	 * 
	 * @param args
	 *            path to an obj-file and desired amount of faces.
	 * @throws FileNotFoundException
	 *             if the specified file cannot be found.
	 * @throws IOException
	 *             if there is an IO-Error during loading the specified file or
	 *             writing the new file.
	 * 
	 */
	public static void main(String[] args) throws FileNotFoundException, IOException {
		printUsage();

		// args[0] = [input file]
		// args[1] = [number of desired faces]
		if (args.length > 2) {
			printUsage();
			System.exit(-1);
		}

		String file = null;
		int desiredAmount = 0;
		HalfedgeMesh3D mesh = null;
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

		if (args.length == 0) {
			System.out.println("please enter the path to an obj-file:");
			file = in.readLine();
		} else {
			file = args[0];
		}

		IFSMesh3D ifsMesh = OBJParser.parseMesh3D(file);

		if (args.length <= 1) {
			System.out.println("please the desired amount of faces:");
			desiredAmount = Integer.parseInt(in.readLine());
		} else {
			desiredAmount = Integer.parseInt(args[1]);
		}
		in.close();

		mesh = new HalfedgeMesh3D(ifsMesh);

		try {
			System.out.println("reducing mesh to " + desiredAmount + " faces");
			mesh.reduceTo(desiredAmount);
		} catch (Exception e) {
			System.out.println("it seems there has been some kind of problem...");
			throw e;
		} finally {
			if (mesh != null) {
				System.out.print("mesh has been reduced to " + mesh.faceAmount + " faces");
				if (mesh.faceAmount != desiredAmount)
					System.out.print(" instead of " + desiredAmount + " faces");
				System.out.println();
				IFSMesh3D output = new IFSMesh3D(mesh);
				OBJParser.serializeMesh3D(output, output.path + File.separatorChar + output.name + "_reduced");
			}
		}
	}

	/**
	 * prints out information on how to use this program correctly.
	 */
	private static void printUsage() {
		System.out.println("usage: " + MeshReduction.class.getName() + " [obj_file] [amount_of_desired_faces]");
	}
}
